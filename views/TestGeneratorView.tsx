import BasicState from "src/libs/basic-view" 
import "../assets/css/style.scss"   
import TestGeneratorForm from "./generator/TestGeneratorForm"

export default class TestGeneratorView extends BasicState {
    props: any 
    addRender = () => {
        return <TestGeneratorForm {...this.props} />   
    }
}